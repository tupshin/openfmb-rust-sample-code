use std::error::Error;
use std::str::FromStr;

extern crate clap;
use clap::{App, Arg};

extern crate prost;
use crate::prost::Message;

extern crate rust_openfmb_ops_protobuf;
use rust_openfmb_ops_protobuf::*;

extern crate natsclient;
use natsclient::{AuthenticationStyle, Client, ClientOptions};

use uuid::Uuid;

extern crate ctrlc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use std::thread;
use std::time;

fn meter_reading_profile_handler(
    msg: &natsclient::Message,
) -> Result<(), natsclient::error::Error> {
    println!("\nSubject: {}", msg.subject);
    println!("Subscription ID: {}", msg.subscription_id);
    println!("Payload Size: {}", msg.payload_size);

    let r = openfmb::metermodule::MeterReadingProfile::decode(msg.payload.to_owned());
    match r {
        Ok(openfmb_message) => {
            let mrid: String;

            let rmi = openfmb_message.reading_message_info;
            // let ied = message.ied;
            // let m = message.meter;
            let mr = openfmb_message.meter_reading;

            if rmi.is_some() {
                let rmi_val = rmi.unwrap();
                let mi = rmi_val.message_info.unwrap();
                let io = mi.identified_object.unwrap();
                mrid = io.m_rid.unwrap();

                let ts = mi.message_time_stamp.as_ref().unwrap();

                // Print the timestamp info
                println!("Message Timestamp: {}.{}", ts.seconds, ts.fraction);
            } else {
                mrid = String::new();
            }

            let ppv_pab = mr
                .unwrap_or_default()
                .reading_mmxu
                .unwrap_or_default()
                .ppv
                .unwrap_or_default()
                .phs_ab
                .unwrap_or_default()
                .c_val
                .unwrap_or_default();
            if ppv_pab.mag.is_some() {
                println!("We have a ReadingMMXU.PPV.PhsAB.cVal.mag value!!!");
                let av = ppv_pab.mag.clone().unwrap();
                if av.f.is_some() {
                    println!("  mag value: {}", av.f.clone().unwrap());
                } else if av.i.is_some() {
                    println!("  mag value: {}", av.i.clone().unwrap());
                }
            } else {
                println!("We DO NOT have a ReadingMMXU.PPV.PhsAB.cVal.mag value...");
            }

            if mrid == "2e0b57d0-6779-4265-98c9-ef60da29fa79" {
                std::process::exit(2);
            }
        }
        Err(e) => {
            // Protobuf did not decode properly!
            println!("{}", e);
        }
    }
    Ok(())
}

fn main() {
    let matches = App::new(env!("CARGO_PKG_NAME"))
        .version(env!("CARGO_PKG_VERSION"))
        .author(env!("CARGO_PKG_AUTHORS"))
        .about(env!("CARGO_PKG_DESCRIPTION"))
        .arg(
            Arg::with_name("SERVER")
                .short("s")
                .long("server")
                .value_name("URL")
                .help("NATS broker server URL")
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::with_name("MRID")
                .short("m")
                .long("mrid")
                .help("mRID of meter to subscribe to for messages")
                .takes_value(true)
                .required(true),
        )
        .get_matches();

    // Gets a value for config if supplied by user, or defaults to "default.conf"
    let nats_server = matches.value_of("SERVER").unwrap();
    println!("Value for SERVER: {}", nats_server);

    let mrid = matches.value_of("MRID").unwrap();

    // Make sure that the MRID parameter is a valid UUIDv4 or a NATS wildcard character
    if mrid == ">" {
        // NATS wildcard character...Ok
    } else {
        match Uuid::from_str(&mrid) {
            Ok(ok) => {
                if ok.get_version_num() != 4 {
                    println!("MRID version is invalid.");
                    std::process::exit(1);
                }
            }
            Err(e) => {
                println!("MRID pararmeter is not valid!\nParseError: {}", e);
                std::process::exit(1);
            }
        }
    }

    let topic = "openfmb.metermodule.MeterReadingProfile.".to_owned() + mrid;
    println!("Subscription topic: {}", &topic);

    let opts = ClientOptions::builder()
        .cluster_uris(vec![nats_server.into()])
        .authentication(AuthenticationStyle::Anonymous)
        .connect_timeout(std::time::Duration::from_millis(300))
        .build()
        .unwrap();

    let client: Client;
    let r = Client::from_options(opts);
    match r {
        Ok(c) => {
            println!("Creation of NATS client successful...");
            client = c;
        }
        Err(e) => {
            println!("{}", e.description());
            std::process::exit(1);
        }
    };
    let cc = client.connect();
    match cc {
        Ok(_cc_ok) => {
            println!("Successfully connected to NATS server...");
        }
        Err(e) => {
            println!("Connection to NATS server failed: {}", e);
            std::process::exit(1);
        }
    }

    let sr = client.subscribe(topic.as_str(), meter_reading_profile_handler);
    match sr {
        Ok(_sr_ok) => {
            println!("Successfully subscribed...");

            let running = Arc::new(AtomicBool::new(true));
            let r = running.clone();
            ctrlc::set_handler(move || {
                r.store(false, Ordering::SeqCst);
            })
            .expect("Error setting Ctrl-C handler");
            println!("Waiting for Ctrl-C...");
            while running.load(Ordering::SeqCst) {
                thread::sleep(time::Duration::from_millis(10));
            }
            println!("Got it! Exiting...");
        }
        Err(e) => {
            println!("{}", e);
        }
    }
}
